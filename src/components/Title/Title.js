import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faClock } from '@fortawesome/free-regular-svg-icons';
import styled from 'styled-components';

const TitleText = styled.h1`
	padding-bottom: 10px;
	margin: 0;
	font-weight: Bold;
	font-size: 24px;
	line-height: 30px;
	border-bottom: 2px solid #A1A2A4;
	svg {
		color: #EEEEEE;
	}
`;

const Title = ({ text }) => {
	return (
		<TitleText><FontAwesomeIcon icon={faClock} /> {text}</TitleText>
	);
}

export default Title;
