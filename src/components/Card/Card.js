import styled from 'styled-components';

const Container = styled.div`
	width: 350px;
	margin: auto;
	padding: 20px;
	border-radius: 10px;
	box-shadow: 0 0 5px #CCCCCC;
	background-color: #FFFFFF;
`;

const Card = ({ children }) => {
	return (
		<Container>{children}</Container>
	);
}

export default Card;
