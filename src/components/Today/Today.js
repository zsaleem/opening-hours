import styled from 'styled-components';

const Container = styled.span`
	margin-left: 15px;
	color: #5BCB02;
	font-size: 12px;
	line-height: 14px;
	font-weight: bold;
	text-transform: uppercase;
`;

const Today = () => {
	return (
		<Container>Today</Container>
	);
}

export default Today;
