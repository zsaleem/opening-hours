import moment from 'moment';
import Today from '../Today/';
import Status from '../Status/';
import styled from 'styled-components';

const Container = styled.span`
	font-weight: bold;
	text-transform: capitalize;
`;

const Day = ({ weekDay }) => {
	const days = [
		'sunday',
		'monday',
		'tuesday',
		'wednesday',
		'thursday',
		'friday',
		'saturday'
	];

	return (
		<Container>
			<span>
				{weekDay}
				{
					days[moment(new Date()).weekday()] === weekDay
					?
					<Today />
					:
					null
				}
			</span>
		</Container>
	);
}

export default Day;
