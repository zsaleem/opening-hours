import useTime from './hooks/';
import moment from 'moment';
import styled from 'styled-components';

const Container = styled.span`
	position: absolute;
	right: 0;
	color: ${({status}) => status ? `#A1A2A4` : `#000000` };
`;

const Status = ({ contents, weekDay }) => {
	const [isClosedNextDay] = useTime(contents, weekDay);

	return (
		<Container status={contents?.length > 0 ? false : true}>
			{
				contents?.length === 0
				?
				'Closed'
				:
				contents?.map((content, index) => (
					<span key={index}>
						{isClosedNextDay(content, index)}
						{
							index === contents.length - 1 || (index === 0 && content?.type === 'close')
							?
							null
							:
								contents.length > 2 && content?.type === 'close'
								?
								', '
								:
								' - '
						}
					</span>
				))
			}
		</Container>
	);
}

export default Status;
