import { useOpeningHoursContext } from '../../../pages/OpeningHours/contexts/OpeningHoursContext';

const useTime = (contents, weekDay) => {
	const { data } = useOpeningHoursContext();

	const convertToMilitaryHours = (seconds) => {
		return convertToMeridiem(new Date(seconds * 1000).toISOString().substr(11, 8));
	};

	const convertToMeridiem = (timeInMilitaryHours) => {
		const militaryHour = +timeInMilitaryHours.substr(0, 2);
		const meridiemHour = militaryHour % 12 || 12;
		const meridiemIndicator = (militaryHour < 12 || militaryHour === 24) ? 'AM' : 'PM';

		return `${meridiemHour} ${meridiemIndicator}`;
	};
	
	const isClosedNextDay = (object, index) => {
		if (contents.length <= 0) {
			return;
		}

		if (object?.type === 'open' && index === contents.length - 1) {
			return contents?.map((content, contentIndex) => {
				if (contentIndex === index) {
					if (object?.type === 'open' && contents[++contentIndex] === undefined) {
						const nextDay = getNextDay(data, weekDay);
						if (nextDay[0]?.type === 'close') {
							return `${convertToMilitaryHours(object.value)} 
												- 
											${convertToMilitaryHours(nextDay[0]?.value)}`;
						}
					}
				}
			});
		} else {
			if (index === 0 && object?.type === 'close') {
				return;
			}
			return convertToMilitaryHours(object.value);
		}
	};

	const getNextDay = (obj, currentKey) => {
    return Object.values(obj)[Object.keys(obj).indexOf(currentKey) + 1];
	};

	return [isClosedNextDay];
};

export default useTime;
