import {
  createContext,
  useContext,
} from 'react';
import data from '../../../data/';

export const openingHoursContext = createContext({
});

export const useOpeningHoursContext = () => useContext(openingHoursContext);

const OpeningHoursProvider = ({ children }) => {
  return (
    <openingHoursContext.Provider
      value={{
        data,
      }}
    >
      {children}
    </openingHoursContext.Provider>
  );
};

export default OpeningHoursProvider;
