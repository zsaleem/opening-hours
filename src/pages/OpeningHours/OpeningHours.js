import { useEffect, useState } from 'react';
import Card from '../../components/Card/';
import Title from '../../components/Title/';
import Day from '../../components/Day/';
import Status from '../../components/Status/';
import styled from 'styled-components';
import { useOpeningHoursContext } from './contexts/OpeningHoursContext';

const Row = styled.div`
	position: relative;
	padding: 10px 0;
	border-bottom: 1px solid #EEEEEE;
`;

const OpeningHours = () => {
	const { data } = useOpeningHoursContext();

	return (
		<Card>
			<Title text='Opening Hours' />
			{
				Object.entries(data).map((item) => (
					<Row key={item[0]}>
						<Day weekDay={item[0]} />
						<Status weekDay={item[0]} contents={item[1]} />
					</Row>
				))
			}
		</Card>
	);
}

export default OpeningHours;
