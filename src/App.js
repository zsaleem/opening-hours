import logo from './logo.svg';
import './App.css';
import OpeningHoursProvider from './pages/OpeningHours/contexts/OpeningHoursContext';
import OpeningHours from './pages/OpeningHours/';

function App() {
  return (
    <OpeningHoursProvider>
      <OpeningHours />
    </OpeningHoursProvider>
  );
}

export default App;
