## Requirements
Below is the stack I used in this project. Please follow the getting started section in order to make this project run.

## Stack
 - React
 - React Hooks
 - React Context API
 - JSX
 - Styled-Components
 - CSS Animations
 - ES6+
 - Git
 - Gitflow
 - Gitlab
 - NPM/Yarn
 - Sublime Text
 - Font Awesome
 - Mac OS X
 - Google Chrome Incognito
 - nodejs(16.9.1)

## Getting started
In order to run this project in the browser successfully, please follow the steps below!

    1. Clone this repository.
    2. cd into `/wolt-hours` directory.
    3. Run `yarn/npm install` command to download and install all dependencies.
    4. To run this project use `yarn start/npm start` command in command line.
    5. To build the project for production run `yarn build/npm build` command. This will build the app for production and put all the files in `/build` folder.

## Description
Above steps, in getting started section, will install all the dependencies required for this project to run.

In this task I used `React 17.0.2`. I made use of `React Hooks`, `React Context API` etc. I wrote the entire code inside `/src` folder. All the components for this project resides in `/src/components/` folder. Every component has its own folder with its file name and alongside dependent components folders if they exist. `/src/components/` folder has all the components that are common to the the entire application for reusability reasons.

### Architecture
I had some options regarding choosing the best architecture for this app. One option was to choose simple components base architecture. I did not choose that because it could turn the project into `speghatti code` which is hard to read, maintain and scale.

`Redux`, a most widely use state management library for react, was another option. However, I did not choose that because `redux` would have overkill for such a small project. I had to write a lot of boilerplate code.

What I chose was `React Context API`. With this approach, I could have a global state with up to date data in it. That data is accessible to all the child components. This architecture is also scalable because all I had to do is to keep adding new components and access the data from global state.

### Development process
The development process that I followed for this project is `Gitflow` work flow. That means, I have two git branches `master` and `develop`. So I created a `feature branch` from `develop` branch. I develop the whole feature in that branch. Once I am done with it. I `merge` that branch with `develop` branch and push it to remote repo with `develop` branch. When I need to release new feature, I create a new branch `release` with a version number on it. Then I merge that `release` branch into master branch and push it to remote repo's `master` branch.

### Notes
I updated the data in `src/data/index.js` file for `Saturday` so that there are multiple opening and closing times.